﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Controls;
using PluginsInterface;

namespace TPALdraw
{
    class StraightTool : Tool
    {

        public override Cursor cursor { get; set; } = Cursors.Cross;
        public override string toolName { get; } = "Prosta";

        public override void drawShape(InkCanvas Canvas) {

            StylusPointCollection pts = new StylusPointCollection();

            pts.Add(new StylusPoint(this.mouseLeftDownPoint.X, this.mouseLeftDownPoint.Y));
            pts.Add(new StylusPoint(this.currentPoint.X, this.currentPoint.Y));
           
            if (this.st != null) Canvas.Strokes.Remove(this.st);

            this.st = new Stroke(pts);

            this.st.DrawingAttributes.Color = Canvas.DefaultDrawingAttributes.Color;
            this.st.DrawingAttributes.Width = Canvas.DefaultDrawingAttributes.Width;
            this.st.DrawingAttributes.Height = Canvas.DefaultDrawingAttributes.Height;

            Canvas.Strokes.Add(this.st);
        }

    }
}
