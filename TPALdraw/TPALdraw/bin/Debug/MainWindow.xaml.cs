﻿using ColorPickerWPF;
using ColorPickerWPF.Code;
using Microsoft.Win32;
using PluginsInterface;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TPALdraw
{

    public partial class MainWindow : Window
    {
        
        PluginsInterface.Tool currentTool = null;
        string fileNameForSave = "";
        StrokeCollection strokesHistory = new StrokeCollection();
        List<string> loadedPlugins = new List<string>();


        public MainWindow()
        {
            
            CultureResources.ChangeCulture(Properties.Settings.Default.DefaultCulture);

            InitializeComponent();
        }

        private void ChangeLanguageMenuItem_Click(object sender, RoutedEventArgs e)
        {

            bool setPolishCulture = (sender == PolishMenuItem);

            PolishMenuItem.IsChecked = setPolishCulture;
            EnglishMenuItem.IsChecked = !setPolishCulture;

            CultureResources.ChangeCulture(new CultureInfo(setPolishCulture ? "pl" : "en"));

        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {

            clearCanvas(Properties.Resources.SureToClearImage, Properties.Resources.ClearingConfirmation);
            
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {

            clearCanvas(Properties.Resources.SureToClearImage, Properties.Resources.ClearingConfirmation);
            
        }

        private void clearCanvas(string message, string title) {
            MessageBoxResult messageBoxResult = MessageBox.Show(message, title, MessageBoxButton.YesNo);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                MainCanvas.Children.Clear();
                MainCanvas.Strokes.Clear();
                strokesHistory.Clear();

                checkRedoButton();
                checkUndoButton();

            }
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            Color color;

            bool ok = ColorPickerWindow.ShowDialog(out color, ColorPickerDialogOptions.SimpleView);

            MainCanvas.DefaultDrawingAttributes.Color = color;

        }

        private void lineThicknessComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!IsLoaded)
            {
                return;
            }

            if (lineThicknessComboBox.SelectedIndex == 0)
            {
                MainCanvas.DefaultDrawingAttributes.Height = 2.0;
                MainCanvas.DefaultDrawingAttributes.Width = 2.0;
            }
            else if (lineThicknessComboBox.SelectedIndex == 1)
            {
                MainCanvas.DefaultDrawingAttributes.Height = 5.0;
                MainCanvas.DefaultDrawingAttributes.Width = 5.0;
            }
            else
            {
                MainCanvas.DefaultDrawingAttributes.Height = 8.0;
                MainCanvas.DefaultDrawingAttributes.Width = 8.0;
            }          

        }

        private void MenuItem_Click_5(object sender, RoutedEventArgs e)
        {
            currentTool = null;
            changeTool(InkCanvasEditingMode.Ink, Cursors.Pen);
        }

        private void MenuItem_Click_3(object sender, RoutedEventArgs e)
        {
            currentTool = new ElipseTool();
            changeTool(InkCanvasEditingMode.None, currentTool.cursor);
        }

        private void MenuItem_Click_4(object sender, RoutedEventArgs e)
        {
            currentTool = new RectangleTool();
            changeTool(InkCanvasEditingMode.None, currentTool.cursor);
        }

        private void MenuItem_Click_6(object sender, RoutedEventArgs e)
        {
            currentTool = new TriangleTool();
            changeTool(InkCanvasEditingMode.None, currentTool.cursor);
        }

        private void MenuItem_Click_7(object sender, RoutedEventArgs e)
        {
            currentTool = new StraightTool();
            changeTool(InkCanvasEditingMode.None, currentTool.cursor);
        }

        private void MenuItem_Click_8(object sender, RoutedEventArgs e)
        {
            currentTool = null;
            changeTool(InkCanvasEditingMode.EraseByPoint);
        }

        private void MainCanvas_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (currentTool != null)
            {
                currentTool.mouseLeftDownPoint = new Point(-1, -1);
                currentTool.st = null;
            }

            checkUndoButton();

        }

        private void MainCanvas_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {

                if (currentTool != null)
                {
                    if (currentTool.mouseLeftDownPoint == new Point(-1, -1)) currentTool.mouseLeftDownPoint = e.GetPosition(MainCanvas);

                    currentTool.currentPoint = e.GetPosition(MainCanvas);

                    currentTool.drawShape(MainCanvas);
                }

            }
        }

        private void changeTool(InkCanvasEditingMode mode, Cursor cursor = null) {

            MainCanvas.EditingMode = mode;
            if (cursor != null)
            {
                MainCanvas.ForceCursor = true;
                MainCanvas.Cursor = cursor;
            }
            else
            {
                MainCanvas.ForceCursor = false;
            }

        }

        /// <summary>
        /// Save
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MenuItem_Click_9(object sender, RoutedEventArgs e)
        {
            if (fileNameForSave.Length > 0)
            {
                doSave(fileNameForSave);
            }
            else
            {
                SaveFileDialog dialog = new SaveFileDialog()
                {
                    Filter = Properties.Resources.SaveOpenFileDialogFilter,
                    FileName = "Obrazek"
                };

                if (dialog.ShowDialog() == true)
                {

                    doSave(dialog.FileName);
                    fileNameForSave = dialog.FileName;
                }
            }
            
        }

        /// <summary>
        /// Open
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MenuItem_Click_10(object sender, RoutedEventArgs e)
        {
            doOpen();
        }
        /// <summary>
        /// SaveAs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MenuItem_Click_11(object sender, RoutedEventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog()
            {
                Filter = Properties.Resources.SaveOpenFileDialogFilter,
                FileName = "Obrazek"
            };

            if (dialog.ShowDialog() == true)
            {

                doSave(dialog.FileName);
                fileNameForSave = dialog.FileName;
            }
        }

        private void doSave(string fileOutputPath) {

            string extension = System.IO.Path.GetExtension(fileOutputPath);

            RenderTargetBitmap rtb = new RenderTargetBitmap((int)MainCanvas.ActualWidth, (int)MainCanvas.ActualHeight, 96d, 96d, PixelFormats.Default);
            rtb.Render(MainCanvas);

            BitmapEncoder encoder = null;

            switch (extension)
            {
                case ".jpg":
                    {
                        encoder = new JpegBitmapEncoder();
                        break;
                    }
                case ".png":
                    {
                        encoder = new PngBitmapEncoder();
                        break;
                    }
                case ".bmp":
                    {
                        encoder = new BmpBitmapEncoder();
                        break;
                    }
                case ".tiff":
                    {
                        encoder = new TiffBitmapEncoder();
                        break;
                    }
                case ".wmp":
                    {
                        encoder = new WmpBitmapEncoder();
                        break;
                    }
                case ".gif":
                    {
                        encoder = new GifBitmapEncoder();
                        break;
                    }
                default:
                    {
                        encoder = new JpegBitmapEncoder();

                        break;
                    }

            }

            encoder.Frames.Add(BitmapFrame.Create(rtb));
            FileStream fs = File.Open(fileOutputPath, FileMode.Create);
            encoder.Save(fs);
            fs.Close();
        }

        private void doOpen() {

            OpenFileDialog dialog = new OpenFileDialog()
            {
                Filter = Properties.Resources.SaveOpenFileDialogFilter
            };

            if (dialog.ShowDialog() == true)
            {
                MainCanvas.Strokes.Clear();
                MainCanvas.Children.Clear();

                fileNameForSave = dialog.FileName;

                BitmapImage toLoad = new BitmapImage();
                toLoad.BeginInit();
                toLoad.CacheOption = BitmapCacheOption.OnLoad;
                toLoad.UriSource = new Uri(dialog.FileName, UriKind.Relative);
                toLoad.EndInit();

                Image image = new Image
                {

                    Source = toLoad,
                    Width = toLoad.Width,
                    Height = toLoad.Height


                };
                MainCanvas.Children.Add(image);
            }
        }

        private void MenuItem_Click_12(object sender, RoutedEventArgs e)
        {
            fileNameForSave = "";
            clearCanvas(Properties.Resources.CreatNewDocument, Properties.Resources.MenuFileNewLabel);
        }

        /// <summary>
        /// Toolbar open
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MenuItem_Click_13(object sender, RoutedEventArgs e)
        {
            doOpen();
        }

        private void Undo_Click(object sender, RoutedEventArgs e)
        {
            if (MainCanvas.Strokes.Count > 0)
            {
                int topItem = MainCanvas.Strokes.Count - 1;

                Stroke workingStroke = MainCanvas.Strokes[topItem];

                strokesHistory.Add(workingStroke);
                MainCanvas.Strokes.Remove(workingStroke);
                Redo.IsEnabled = true;
            }

            checkUndoButton();

        }

        private void Redo_Click(object sender, RoutedEventArgs e)
        {
            if (strokesHistory.Count > 0)
            {
                int topItem = strokesHistory.Count - 1;
                Stroke workingStroke = strokesHistory[topItem];

                MainCanvas.Strokes.Add(workingStroke);
                strokesHistory.Remove(workingStroke);
            }

            checkRedoButton();
            checkUndoButton();

        }

        private void checkRedoButton() {
            if (strokesHistory.Count > 0)
            {
                Redo.IsEnabled = true;
                Redo.Icon = this.Resources["RedoIcon"] as Image;
            }
            else
            {
                Redo.IsEnabled = false;
                Redo.Icon = this.Resources["disabledRedoIcon"] as Image;
            }
        }

        private void checkUndoButton()
        {
            if (MainCanvas.Strokes.Count > 0)
            {
                Undo.IsEnabled = true;
                Undo.Icon = this.Resources["UndoIcon"] as Image;
            }
            else
            {
                Undo.IsEnabled = false;
                Undo.Icon = this.Resources["disabledUndoIcon"] as Image;
            }
        }

        public void loadAllPlugins() {

            string[] fileEntries = Directory.GetFiles(@"Plugins");

            foreach (string fileName in fileEntries) loadPlugin(fileName);
          
        }

        public bool loadPlugin(string path) {

            var a = Assembly.LoadFile(System.IO.Path.GetFullPath(path));

            foreach (var t in a.GetTypes())
            {
                if (typeof(PlugInterface).IsAssignableFrom(t))
                {
                    var c = Activator.CreateInstance(t);

                    var w = (PlugInterface)c;

                    if (!loadedPlugins.Contains(w.toolName))
                    {

                        MenuItem plug = new MenuItem();
                        plug.Header = w.toolName;
                        plug.Icon = this.Resources["PluginIcon"] as Image;
                        plug.Click += (s, e) => { w.drawShape(MainCanvas); };
                        pluginsAndExtensions.Items.Add(plug);
                        loadedPlugins.Add(w.toolName);
                        return true;
                    }
                }
            }

            return false;

        }

        private void MenuItem_Click_14(object sender, RoutedEventArgs e)
        {
            string plugin = importDll();

            bool result = false;
            if (plugin == "ERR_DIALOG_CANCELED") return;
            if (plugin == "ERR_PLUGIN_EXISTS")
            {
                MessageBox.Show(Properties.Resources.PluginAlreadyExists);
                return;
            }

            result = loadPlugin(plugin);

            if (result) MessageBox.Show(Properties.Resources.PluginAdded); else MessageBox.Show(Properties.Resources.PluginFailed);

        }

        private string importDll()
        {

            OpenFileDialog dialog = new OpenFileDialog()
            {
                Filter = Properties.Resources.PluginFilter
            };

            if (dialog.ShowDialog() == true)
            {
                string[] path = dialog.FileName.Split('\\');

                if (File.Exists(@"Plugins\" + path[path.Length - 1])) return "ERR_PLUGIN_EXISTS";

                if (!Directory.Exists(@"Plugins")) Directory.CreateDirectory(@"Plugins");
                
                File.Copy(dialog.FileName, @"Plugins\" + path[path.Length-1]);
                return @"Plugins\" + path[path.Length - 1];
                
            }

            return "ERR_DIALOG_CANCELED";

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            loadAllPlugins();
        }

        private void MenuItem_Click_15(object sender, RoutedEventArgs e)
        {
            ChangeLanguageMenuItem_Click(sender, e);
        }

        private void EnglishMenuItem_Click(object sender, RoutedEventArgs e)
        {
            ChangeLanguageMenuItem_Click(sender, e);
        }

        private void MenuItem_Click_16(object sender, RoutedEventArgs e)
        {
            UpdatesWindow upwin = new UpdatesWindow();
            upwin.Show();
        }

        private void MenuItem_Click_17(object sender, RoutedEventArgs e)
        {
            if (MainCanvas.Strokes.Count > 0)
            {
                int topItem = MainCanvas.Strokes.Count - 1;

                Stroke workingStroke = MainCanvas.Strokes[topItem];

                strokesHistory.Add(workingStroke);
                MainCanvas.Strokes.Remove(workingStroke);
                Redo.IsEnabled = true;
            }

            checkUndoButton();
        }

        private void MenuItem_Click_18(object sender, RoutedEventArgs e)
        {
            if (strokesHistory.Count > 0)
            {
                int topItem = strokesHistory.Count - 1;
                Stroke workingStroke = strokesHistory[topItem];

                MainCanvas.Strokes.Add(workingStroke);
                strokesHistory.Remove(workingStroke);
            }

            checkRedoButton();
            checkUndoButton();
        }

        private void MenuItem_Click_19(object sender, RoutedEventArgs e)
        {
            currentTool = null;
            changeTool(InkCanvasEditingMode.Ink, Cursors.Pen);
        }

        private void MenuItem_Click_20(object sender, RoutedEventArgs e)
        {
            currentTool = new ElipseTool();
            changeTool(InkCanvasEditingMode.None, currentTool.cursor);
        }

        private void MenuItem_Click_21(object sender, RoutedEventArgs e)
        {
            currentTool = new RectangleTool();
            changeTool(InkCanvasEditingMode.None, currentTool.cursor);
        }

        private void MenuItem_Click_22(object sender, RoutedEventArgs e)
        {
            currentTool = new TriangleTool();
            changeTool(InkCanvasEditingMode.None, currentTool.cursor);
        }

        private void MenuItem_Click_23(object sender, RoutedEventArgs e)
        {
            currentTool = new StraightTool();
            changeTool(InkCanvasEditingMode.None, currentTool.cursor);
        }

        private void MenuItem_Click_24(object sender, RoutedEventArgs e)
        {
            currentTool = null;
            changeTool(InkCanvasEditingMode.EraseByPoint);
        }
    }
}
