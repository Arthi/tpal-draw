﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace TPALdraw
{
    public partial class UpdatesWindow : Window
    {
        CancellationTokenSource cts;

        public UpdatesWindow()
        {
            InitializeComponent();
        }

        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {

            progressBar.Value = 0;

            var progressIndicator = new Progress<int>(ReportProgress);

            cts = new CancellationTokenSource();

            await checkForUpdatesAsync(progressIndicator, cts.Token);

        }

        private void ReportProgress(int value)
        {

            progressText.Content = value.ToString() + "%";
            progressBar.Value++;

        }

        private void handleCancelationButton(int value)
        {
            this.Close();
        }

        private void handleOperationFinish()
        {
            MessageBox.Show(Properties.Resources.NoUpdatesAvailable);
            this.Close();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            cts.Cancel();
        }

        //nasz zapoytach
        private async Task<int> checkForUpdatesAsync(IProgress<int> progress, CancellationToken ct)
        {

            int elem = 0;

            int processCount = await Task.Run<int>(() =>
            {

                try
                {

                    for (elem = 0; elem <= 100; elem++)
                    {

                        Thread.Sleep(50);

                        if (progress != null)
                        {
                            progress.Report(elem);
                        }

                        ct.ThrowIfCancellationRequested();

                    }

                    this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() => { handleOperationFinish(); }));

                }
                catch (OperationCanceledException)
                {
                    this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() => { handleCancelationButton(elem); }));
                }

                return elem;
            });

            return processCount;

        }
    }
}
