﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace TPALdraw
{

    public class CultureResources
    {
        private static ObjectDataProvider resourceProvider = (ObjectDataProvider) Application.Current.FindResource("Resources");

        

        public Properties.Resources GetResourceInstance()
        {
            return new Properties.Resources();
        }

        public static void ChangeCulture (CultureInfo culture)
        {
            Properties.Resources.Culture = culture;
            resourceProvider.Refresh();
        }
    }

}
