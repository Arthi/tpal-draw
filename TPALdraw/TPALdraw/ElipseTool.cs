﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Controls;
using PluginsInterface;

namespace TPALdraw
{
    class ElipseTool : Tool
    {
        public override Cursor cursor { get; set; } = Cursors.Cross;
        public override string toolName { get; } = "Elipsa";

        public override void drawShape(InkCanvas Canvas) {

            StylusPointCollection pts = new StylusPointCollection();

            for (double i = 0; i <= 2 * Math.PI; i += 0.01)
            { 
                var x = this.mouseLeftDownPoint.X + (this.mouseLeftDownPoint - this.currentPoint).Length * Math.Cos(i);
                var y = this.mouseLeftDownPoint.Y + (this.mouseLeftDownPoint - this.currentPoint).Length * Math.Sin(i);

                pts.Add(new StylusPoint(x, y));

            }
            
            if (this.st != null) Canvas.Strokes.Remove(this.st);

            this.st = new Stroke(pts);

            this.st.DrawingAttributes.Color = Canvas.DefaultDrawingAttributes.Color;
            this.st.DrawingAttributes.Width = Canvas.DefaultDrawingAttributes.Width;
            this.st.DrawingAttributes.Height = Canvas.DefaultDrawingAttributes.Height;

            Canvas.Strokes.Add(this.st);
        }


    }
}
