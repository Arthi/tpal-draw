﻿using PluginsInterface;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace GrayscalePlugin
{
    public class GrayscalePlugin : Tool
    {

        public override Cursor cursor { get; set; } = Cursors.Cross;
        public override string toolName { get; } = "Skala szarości / Grayscale";
        public override void drawShape(InkCanvas Canvas)
        {

            RenderTargetBitmap rtb = new RenderTargetBitmap((int)Canvas.ActualWidth, (int)Canvas.ActualHeight, 96d, 96d, PixelFormats.Default);
            rtb.Render(Canvas);
            BitmapEncoder encoder = new BmpBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(rtb));

            using (MemoryStream ms = new MemoryStream())
            {
                encoder.Save(ms);
                if (File.Exists(@"grayscale_curren_image_tmp.bmp")) File.Delete(@"grayscale_curren_image_tmp.bmp");
                MakeGrayscale(new Bitmap(ms)).Save(@"grayscale_curren_image_tmp.bmp");
            }
          
            Canvas.Strokes.Clear();
            Canvas.Children.Clear();

            BitmapImage toLoad = new BitmapImage();
            toLoad.BeginInit();
            toLoad.CacheOption = BitmapCacheOption.OnLoad;
            toLoad.UriSource = new Uri(@"grayscale_curren_image_tmp.bmp", UriKind.Relative);
            toLoad.EndInit();

            System.Windows.Controls.Image image = new System.Windows.Controls.Image
            {

                Source = toLoad,
                Width = toLoad.Width,
                Height = toLoad.Height


            };
            Canvas.Children.Add(image);

            File.Delete(@"grayscale_curren_image_tmp.bmp");
            
        }

        private System.Windows.Controls.Image ConvertDrawingImageToWPFImage(System.Drawing.Image gdiImg)
        {
            
            System.Windows.Controls.Image img = new System.Windows.Controls.Image();
            Bitmap bmp = new Bitmap(gdiImg);
            IntPtr hBitmap = bmp.GetHbitmap();
            ImageSource WpfBitmap = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(hBitmap, IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
            img.Source = WpfBitmap;
            img.Width = gdiImg.Width;
            img.Height = gdiImg.Height;
            return img;
        }

        public static Bitmap MakeGrayscale(Bitmap original)
        {
            Bitmap newBitmap = new Bitmap(original.Width, original.Height);
            Graphics g = Graphics.FromImage(newBitmap);
            ColorMatrix colorMatrix = new ColorMatrix(
               new float[][]
              {
                 new float[] {.3f, .3f, .3f, 0, 0},
                 new float[] {.59f, .59f, .59f, 0, 0},
                 new float[] {.11f, .11f, .11f, 0, 0},
                 new float[] {0, 0, 0, 1, 0},
                 new float[] {0, 0, 0, 0, 1}
              });

            ImageAttributes attributes = new ImageAttributes();
            attributes.SetColorMatrix(colorMatrix);
            g.DrawImage(original, new Rectangle(0, 0, original.Width, original.Height),
               0, 0, original.Width, original.Height, GraphicsUnit.Pixel, attributes);
            g.Dispose();
            return newBitmap;
        }


    }
}
