﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Ink;
using System.Windows.Input;

namespace PluginsInterface
{
    public interface PlugInterface
    {
        string toolName { get; }

        void drawShape(InkCanvas Canvas);

    }
}
