﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Ink;
using System.Windows.Input;

namespace PluginsInterface
{
    public abstract class Tool : PlugInterface
    {

        public Point mouseLeftDownPoint { get; set; }
        public Point currentPoint { get; set; }
        public Stroke st { get; set; }
        public abstract Cursor cursor { get; set; }
        public abstract string toolName { get; }

        public Tool()
        {
            mouseLeftDownPoint = new Point(-1, -1);
            currentPoint = new Point(-1, -1);
            st = null;
        }

        public abstract void drawShape(InkCanvas Canvas);

    }
}
